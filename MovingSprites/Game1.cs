﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace MovingSprites
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D spaceship;
        KeyboardState keyState;
        Vector2 position, velocity;
        float speed = 80f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //set the default game resolution.
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;


            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            //Initialize the game by getting the keyboard state.
            keyState = Keyboard.GetState();
            //Initially set velocity to zero
            velocity = Vector2.Zero;
            //initialize the position to be 100,200.
            position = new Vector2(100, 200);


            base.Initialize();

           
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //Loads the png asset into the Texture2D
            spaceship = Content.Load<Texture2D>("PlayerPaper");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            //Unloads the Texture2D
            spaceship.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //We poll the keyboard on every update.
            keyState = Keyboard.GetState();


            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyState.IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here


            //Position X = Cos(angle in radians)*speed
            //Position Y = Sin(angle in radians)*speed
            float? angle = null;
            if (keyState.IsKeyDown(Keys.Up))
            {
                angle = 3.0f * MathHelper.PiOver2;     
            }
       
            if (keyState.IsKeyDown(Keys.Down))
            {
                angle = MathHelper.PiOver2;   
            }

            if (keyState.IsKeyDown(Keys.Left))
            {
                angle = MathHelper.Pi;
            }

            if (keyState.IsKeyDown(Keys.Right))
            {
                angle = 0;
            }
            //If the angle is not null set the speed accordingly else set the speed to zero;
            if (angle.HasValue)
            {
                velocity = new Vector2((float)Math.Cos(angle.Value) * speed, (float)Math.Sin(angle.Value) * speed);
            }
            else
            {
                velocity = Vector2.Zero;
            }

            //position = position + (velocity*time)
            //Update the position based on time elapsed NOT FRAMES.
            position = Vector2.Add(position, Vector2.Multiply(velocity, (float)gameTime.ElapsedGameTime.TotalSeconds));

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            //To draw something it needs to be between a begin and and end.
            spriteBatch.Begin();
            //Draw the Sprite at the top left corner (Vector2.Zero) of the screen with no tint applied to the sprite (Color.white).
            spriteBatch.Draw(spaceship, position, Color.White);
            //End the sprite batch.
            spriteBatch.End();



            base.Draw(gameTime);
        }
    }
}